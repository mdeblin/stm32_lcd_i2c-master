#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_adc.h"

void adc_init()
{
  GPIO_InitTypeDef GPIO_InitStructure;
  ADC_InitTypeDef  ADC_InitStructure;

  /* Enable ADC1 and GPIOA clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 | RCC_APB2Periph_GPIOA, ENABLE);

  /* Configure PA.01 (ADC Channel1) as analog input -------------------------*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

 // ��������� ADC
 ADC_StructInit(&ADC_InitStructure);
 ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; // ����� ������ - ���������, �����������
 ADC_InitStructure.ADC_ScanConvMode = DISABLE; // �� ����������� ������, ������ �������� ���� �����
 ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; // ����������� ���������
 ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; // ��� �������� ��������
 ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; //������������ ����� ��������� - ������� ������
 ADC_InitStructure.ADC_NbrOfChannel = 1; //���������� ������� - ���� �����
 ADC_Init(ADC1, &ADC_InitStructure);
 ADC_Cmd(ADC1, ENABLE);

 // ��������� ������
 ADC_RegularChannelConfig(ADC1, ADC_Channel_8, 1, ADC_SampleTime_55Cycles5);

 // ���������� ���
 ADC_ResetCalibration(ADC1);
 while (ADC_GetResetCalibrationStatus(ADC1));
 ADC_StartCalibration(ADC1);
 while (ADC_GetCalibrationStatus(ADC1));
}

uint16_t get_adc_value()
{
 ADC_SoftwareStartConvCmd(ADC1, ENABLE);
 while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
 return ADC_GetConversionValue(ADC1);
}
